//MSE 2022-III INFOTEC Matemáticas Discretas  
//Prof. Daniel A. Cervantes
//Tarea 2. Unidad 1. Relaciones y Funciones.

#include <iostream>
using namespace std;
#define MAX_ELEMS  100

struct Monomial{
    float val;
    char var_name; // 'x'
    int exp;   //x^exp
};

class Polynomial{
  Monomial pol [MAX_ELEMS];
  int nTerms;
  float a;
  float b;
  int nMonomials;
  
  public:
    Polynomial(){nMonomials = 0;} 
    //void addMonomial(char var, int exp); // Daniel
    //float evalue(float);     //Todos
    //void add(const Polynomial& pol1, Polynomial& out_pol2); //Todos 
    //void diff(const Polynomial& pol1, Polynomial& out_pol2); //Todos
    //void mult(const Polynomial& pol1, Polynomial& out_pol2); //Todos
    //void div(const Polynomial& pol1, Polynomial& out_pol2);
    //void derivate(Polynomial& out_pol2);
    //float integral();
    //void graph(int eval);   
    //void print(); //Daniel  
};

