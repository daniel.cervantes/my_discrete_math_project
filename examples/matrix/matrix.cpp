#include <num/Matrix.hpp>

int main(){
  int n = 10, m = 10;
  Matrix a(n,m),b(n,m);
  
  srand((unsigned) time(NULL));
  
  
  for(int i=0; i < n; ++i)
    for(int j=0; j < m; ++j){
      a.set(i,j,rand()%10);
      b.set(i,j,rand()%10);
    }
  
  a.print();
  b.print();
  


  return 0;
}
